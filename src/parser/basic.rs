use std::{collections::{VecDeque, HashSet}, rc::Rc, fmt};

use log::info;

use crate::{mg::*, mcfg::*};



#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Parse<'a> {
    nonterminal: Category,
    range_vectors: Vec<(usize, &'a [&'a str], usize)>,
    comment: Option<String>,
    parents: Vec<Parse<'a>>,
}



impl<'a> Parse<'a> {
    fn fmt_with_prefix(&self, f: &mut fmt::Formatter<'_>, prefix: &str) -> fmt::Result {
        let ranges = self.range_vectors.iter()
            .map(|&(begin, slice, end)| {
                let slice = slice.iter()
                    .map(|&s| s.to_owned())
                    .reduce(|s1, s2| format!("{} {}", s1, s2))
                    .unwrap_or_else(|| String::new());

                format!("{} \"{}\" {}", begin, slice, end)
            })
            .reduce(|s1, s2| format!("{}, {}", s1, s2))
            .unwrap_or_else(|| String::new());

        match self.parents.is_empty() {
            true => {
                write!(f, "{}{} [{}]\n", prefix, self.nonterminal, ranges)
            },

            false => {
                write!(f, "{}┌───────────────────────────────\n", prefix)?;
                let new_prefix = format!("{}│ ", prefix);
                for parent in self.parents.iter() {
                    parent.fmt_with_prefix(f, &new_prefix)?;
                }
                write!(f, "{}├─────────────────────────────── {}\n", prefix, self.comment.as_ref().map(|s| s.as_str()).unwrap_or_else(|| ""))?;
                write!(f, "{}└ {} [{}]\n", prefix, self.nonterminal, ranges)
            },
        }
    }

    fn from_item_and_input(item: &Item, input: &'a [&'a str]) -> Self {
        let range_vectors = item.range_vectors.iter()
            .map(|&(begin, end)| {
                (begin, &input[begin..end], end)
            })
            .collect();

        let parents = item.parents.iter()
            .map(|parent| Parse::from_item_and_input(parent, input))
            .collect();

        Self {
            nonterminal: item.nonterminal.clone(),
            range_vectors,
            comment: item.comment.clone(),
            parents,
        }
    }
}



impl<'a> fmt::Display for Parse<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.fmt_with_prefix(f, "")
    }
}



#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Item {
    nonterminal: Category,
    range_vectors: Vec<(usize, usize)>,
    comment: Option<String>,
    parents: Vec<Rc<Item>>,
}



impl fmt::Display for Item {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {:?}", self.nonterminal, self.range_vectors)
    }
}



fn calc_range_vector(items: &Vec<Rc<Item>>, idxs: &Vec<Vec<IndexPair>>) -> Option<Vec<(usize, usize)>> {
    idxs.iter()
        .fold(
            Some(vec![]),
            |acc, pairs| {
                acc.and_then(|mut acc| {
                    let new_range = pairs.iter()
                        .map(|pair| Some(items[pair.nt_idx].range_vectors[pair.tup_idx]))
                        .reduce(
                            |pair1, pair2| {
                                match (pair1, pair2) {
                                    (Some((begin1, end1)), Some((begin2, end2))) => {
                                        match end1 == begin2 {
                                            true => Some((begin1, end2)),
                                            false => None,
                                        }
                                    },
                                    _ => None,
                                }
                            }
                        )
                        .unwrap();

                    match new_range {
                        Some(new_range) => {
                            acc.push(new_range);
                            Some(acc)
                        },
                        None => None,
                    }
                })
            }
        )
}



pub fn parse<'a>(input: &'a [&'a str], mcfg: &MCFG<String, Category>) -> Option<Parse<'a>> {
    let mut chart: HashSet<Rc<Item>> = HashSet::new();
    let mut agenda: VecDeque<Rc<Item>> = VecDeque::new();

    info!("Input length: {}", input.len());

    for k in 0..input.len() {
        info!("Processing k = {}, input[k] = {:?}...", k, input[k]);

        //  Add axioms to C and A.
        for rule in mcfg.rules.iter() {
            if let Rule::Terminal { lhs, rhs } = rule {
                if rhs.is_empty() {
                    let new_item = Item {
                        nonterminal: lhs.clone(),
                        range_vectors: vec![ (k, k) ],
                        parents: vec![],
                        comment: None,
                    };

                    info!("Axiom: add empty item to chart and agenda: {}", new_item);
                    info!("       due to the MCFG rule {}", rule);

                    chart.insert(Rc::new(new_item.clone()));
                    agenda.push_back(Rc::new(new_item));

                    if k == (input.len() - 1) {
                        let new_item = Item {
                            nonterminal: lhs.clone(),
                            range_vectors: vec![ (k+1, k+1) ],
                            parents: vec![],
                            comment: None,
                        };

                        info!("Axiom: add empty item to chart and agenda: {}", new_item);
                        info!("       due to the MCFG rule {}", rule);

                        chart.insert(Rc::new(new_item.clone()));
                        agenda.push_back(Rc::new(new_item));
                    }
                }
                else if input[k] == *rhs {
                    let new_item = Item {
                        nonterminal: lhs.clone(),
                        range_vectors: vec![ (k, k+1) ],
                        parents: vec![],
                        comment: None,
                    };

                    info!("Axiom: add item to chart and agenda: {}", new_item);
                    info!("       due to the MCFG rule {}", rule);

                    chart.insert(Rc::new(new_item.clone()));
                    agenda.push_back(Rc::new(new_item));
                }
            }
        }

        info!("Chart after axioms:");
        for item in chart.iter() {
            info!(" -  {}", item);
        }

        //  Process the agenda.
        while let Some(item1) = agenda.pop_front() {
            info!("Testing agenda item for complete: {}", item1);

            let mut add_to_chart = vec![];

            //  Complete?
            for rule in mcfg.rules.iter() {
                if let Rule::NonTerminal { lhs, rhs, idxs, comment } = rule {
                    //  Does this rule involve the agenda item?
                    if !rhs.contains(&item1.nonterminal) {
                        continue;
                    }

                    //  Maps each RHS non-terminal to a list of eligible items.
                    let rhs_items = rhs.iter()
                        .fold(
                            Some(Vec::<Vec<Rc<Item>>>::new()),
                            |acc, nt| {
                                acc.and_then(|mut acc| {
                                    let nt_items: Vec<Rc<Item>> = chart.iter()
                                        .filter(|&item| item.nonterminal == *nt)
                                        .map(|item| Rc::clone(item))
                                        .collect();

                                    match nt_items.is_empty() {
                                        true => None,
                                        false => {
                                            acc.push(nt_items);
                                            Some(acc)
                                        },
                                    }
                                })
                            }
                        );

                    //  If any of the RHS non-terminals was not found, skip this rule.
                    if let None = rhs_items {
                        continue;
                    }
                    let rhs_items = rhs_items.unwrap();

                    // info!("RHS match for rule {}", rule);
                    // info!("rhs_items: {:?}", rhs_items);

                    //  A list of combination of eligible items.
                    let item_combos: Vec<Vec<Rc<Item>>> = rhs_items.into_iter()
                        .fold(
                            vec![ vec![] ],
                            |item_combos, items| {
                                items.into_iter()
                                    .map(|item| {
                                        item_combos.clone().into_iter()
                                            .map(move |mut item_combo| {
                                                item_combo.push(Rc::clone(&item));
                                                item_combo
                                            })
                                    })
                                    .flatten()
                                    .collect()
                            }
                        );
                    // info!("item_combos: {:?}", item_combos);

                    for item_combo in item_combos {
                        // info!("Calculate range vec with items {:?} and idxs {:?}", item_combo, idxs);
                        if let Some(new_range_vec) = calc_range_vector(&item_combo, idxs) {
                            // info!("Success!");

                            let new_item = Item {
                                nonterminal: lhs.clone(),
                                range_vectors: new_range_vec,
                                parents: item_combo,
                                comment: comment.clone(),
                            };

                            let new_item_exists = chart.iter()
                                .any(|item| {
                                    //  The parents don't have to match!
                                    (item.nonterminal == new_item.nonterminal) &&
                                    (item.range_vectors == new_item.range_vectors)
                                });

                            if !new_item_exists {
                                info!("Complete: add item to chart and agenda: {}", new_item);
                                info!("          due to the MCFG rule {}", rule);

                                add_to_chart.push(Rc::new(new_item.clone()));
                                agenda.push_back(Rc::new(new_item));
                            }
                        }
                    }
                }
            }

            chart.extend(add_to_chart);
        }

        //  Does chart contain goal?
        //  Todo: this is too eager.  Need to find all parses!
        let goal = chart.iter()
            .find(|&item| {
                (item.nonterminal == mcfg.start_symbol) &&
                (item.range_vectors == &[ (0, input.len()) ])
            });

        if let Some(goal) = goal {
            return Some(Parse::from_item_and_input(goal, input));
        }

        info!("Chart after completes:");
        for item in chart.iter() {
            info!(" -  {}", item);
        }
    };

    None
}