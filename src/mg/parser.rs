use logos::{ Logos, Lexer };

use crate::mg::{ MG, Feature, LexicalEntry };

use std::collections::HashSet;



#[derive(Logos, Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[logos(skip r"[ \t\n\f]+")]
enum Token {
    #[token("::")]
    DoubleColon,

    #[regex("[_a-zA-Z0-9]+")]
    Chars,

    #[token("=")]
    Equals,

    #[token("+")]
    Plus,

    #[token("-")]
    Minus,
}



#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum ParserState {
    Start,
    DoubleColon,
    Feature,
    Selector,
    Licensor,
    Licensee,
    End,
}



impl ParserState {
    fn advance(self, lex: &mut Lexer<'_, Token>) -> Option<(Self, Option<PartialResult>)> {
        match self {
            Self::Start => {
                match lex.next() {
                    Some(Ok(Token::Chars)) => {
                        let s = lex.slice().to_owned();
                        let res = PartialResult::String(s);
                        Some((Self::DoubleColon, Some(res)))
                    },
                    Some(Ok(Token::DoubleColon)) => {
                        let res = PartialResult::String(String::new());
                        Some((Self::Feature, Some(res)))
                    },
                    _ => None,
                }
            },

            Self::DoubleColon => {
                match lex.next() {
                    Some(Ok(Token::DoubleColon)) => {
                        Some((Self::Feature, None))
                    },
                    _ => None,
                }
            },

            Self::Feature => {
                match lex.next() {
                    Some(Ok(Token::Chars)) => {
                        let s = lex.slice().to_owned();
                        let res = PartialResult::Feature(Feature::Selectee(s));
                        Some((Self::Feature, Some(res)))
                    },
                    Some(Ok(Token::Equals)) => {
                        Some((Self::Selector, None))
                    },
                    Some(Ok(Token::Plus)) => {
                        Some((Self::Licensor, None))
                    },
                    Some(Ok(Token::Minus)) => {
                        Some((Self::Licensee, None))
                    },
                    None => {
                        Some((Self::End, None))
                    }
                    _ => None,
                }
            },

            Self::Selector => {
                match lex.next() {
                    Some(Ok(Token::Chars)) => {
                        let s = lex.slice().to_owned();
                        let res = PartialResult::Feature(Feature::Selector(s));
                        Some((Self::Feature, Some(res)))
                    },
                    _ => None,
                }
            },

            Self::Licensor => {
                match lex.next() {
                    Some(Ok(Token::Chars)) => {
                        let s = lex.slice().to_owned();
                        let res = PartialResult::Feature(Feature::Licensor(s));
                        Some((Self::Feature, Some(res)))
                    },
                    _ => None,
                }
            },

            Self::Licensee => {
                match lex.next() {
                    Some(Ok(Token::Chars)) => {
                        let s = lex.slice().to_owned();
                        let res = PartialResult::Feature(Feature::Licensee(s));
                        Some((Self::Feature, Some(res)))
                    },
                    _ => None,
                }
            },

            Self::End => None,
        }
    }



    fn log_error(&self) {
        let expect_what = match self {
            Self::Start => Some("characters or \"::\""),
            Self::DoubleColon => Some("\"::\""),
            Self::Feature => Some("characters, \"=\", \"+\" or \"-\""),
            Self::Selector => Some("characters"),
            Self::Licensor => Some("characters"),
            Self::Licensee => Some("characters"),
            Self::End => None,
        };

        if let Some(expect_what) = expect_what {
            log::error!("Line ended, but expected {}", expect_what);
        }
    }
}



#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum PartialResult {
    String(String),
    Feature(Feature),
}



pub fn parse<S: AsRef<str>>(s: S) -> Option<MG<String>> {
    let mut lines = s.as_ref().lines();

    //  Parse the first line.
    let first_line = lines.next()?;
    let mut first_line_lex = Token::lexer(first_line);

    //  The first line should start with the start selectee...
    let (start_selectee, end) = match first_line_lex.next() {
        Some(Ok(Token::Chars)) => {
            (first_line_lex.slice().to_owned(), first_line_lex.span().end)
        },
        _ => {
            log::error!("First line should contain just the start selectee, got unexpected input \"{}\"", first_line);
            return None;
        },
    };

    //  ... and contain nothing else.
    if first_line_lex.next().is_some() {
        log::error!("First line should contain just the start selectee, got trailing garbage \"{}\"", &first_line[end..]);
        return None;
    }

    let mut alphabet: HashSet<String> = HashSet::new();
    let mut features: HashSet<Feature> = HashSet::new();
    let mut lexicon: Vec<LexicalEntry<String>> = vec![];

    //  Parse the remaining lines.
    for line in lines {
        let mut line_lex = Token::lexer(line);

        let mut item: Option<String> = None;
        let mut fseq: Vec<Feature> = vec![];

        let mut state = ParserState::Start;
        while let Some((new_state, res)) = state.advance(&mut line_lex) {
            state = new_state;

            match res {
                Some(PartialResult::String(s)) => {
                    alphabet.insert(s.clone());
                    item = Some(s);
                },
                Some(PartialResult::Feature(f)) => {
                    features.insert(f.clone());
                    fseq.push(f);
                },
                _ => {},
            };
        }

        if !matches!(state, ParserState::End) {
            state.log_error();
            return None;
        }

        lexicon.push(LexicalEntry { item: item.unwrap(), fseq });
    }

    Some(MG {
        alphabet: alphabet.into_iter().collect(),
        features: features.into_iter().collect(),
        lexicon,
        start_selectee
    })
}