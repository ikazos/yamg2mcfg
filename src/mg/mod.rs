pub mod parser;


use std::collections::BTreeSet;
use std::fmt;

use serde::{Serialize, Deserialize};



/// Features.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum Feature {
    /// Selector feature, e.g. `=v`.
    Selector(String),
    /// Selectee feature, e.g. `v`.
    Selectee(String),
    /// Licensor feature, e.g. `+wh`.
    Licensor(String),
    /// Licensee feature, e.g. `-wh`.
    Licensee(String),
}



impl fmt::Display for Feature {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Feature::Selector(ref x) => write!(f, "={}", x),
            Feature::Selectee(ref x) => write!(f, "{}", x),
            Feature::Licensor(ref x) => write!(f, "+{}", x),
            Feature::Licensee(ref x) => write!(f, "-{}", x),
        }
    }
}



/// Helper struct for printing `&[Feature]`.
pub struct FeatureSliceDisplay<'a>(pub &'a [Feature]);



impl<'a> fmt::Display for FeatureSliceDisplay<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (idx, feature) in self.0.iter().enumerate() {
            if idx > 0 {
                write!(f, " ")?;
            }
            write!(f, "{}", feature)?;
        }

        Ok(())
    }
}



/// Two types of items: lexical or derived.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum Type {
    Lexical,
    Derived,
}



/// Lexical entry.
/// 
/// A lexical entry is a 3-tuple $ \langle I, T, F \rangle $ consisting of the item $ F $,
/// the type $ T $ and the feature sequence $ F $.  Since all lexical entries
/// have a lexical type, the type $ T $ is omitted from the struct definition.
/// 
/// `S` is the type of the item.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct LexicalEntry<S> {
    pub item: S,
    pub fseq: Vec<Feature>,
}



/// Minimalist Grammar.
/// 
/// A Minimalist Grammar (MG) is a 3-tuple $ \langle \Sigma, F, L \rangle $ consisting of the
/// alphabet $ \Sigma $, the feature inventory $ F $ and the lexicon $ L $.
/// 
/// `S` is the type of the items of the lexical entries.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct MG<S> {
    pub alphabet: Vec<S>,
    pub features: Vec<Feature>,
    pub lexicon: Vec<LexicalEntry<S>>,
    pub start_selectee: String,
}



/// A MG category, which is an expression with all the "phonological information" stripped.
/// 
/// A MG category is a tuple $ \langle T, F_0, F_1, \cdots F_n \rangle $ consisting of the type $ T $ (derived or lexical), and the feature sequences $ F_0, F_1, \cdots F_n $.  We call $ F_0 $ the head feature sequence, and the $ F_1, \cdots F_n $ the tail feature sequences.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct Category {
    pub r#type: Type,
    pub head_fseq: Vec<Feature>,
    pub tail_fseqs: BTreeSet<Vec<Feature>>,
}



impl fmt::Display for Category {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let fmt_ty = match self.r#type {
            Type::Derived => ":",
            Type::Lexical => "::",
        };
    
        let fmt_tail_fseqs = self.tail_fseqs.iter()
            .map(|fseq: &Vec<Feature>| format!(", {}", FeatureSliceDisplay(fseq)))
            .fold(format!(""), |s1, s2| format!("{}{}", s1, s2));
    
        write!(f, "({} {}{})", fmt_ty, FeatureSliceDisplay(&self.head_fseq), fmt_tail_fseqs)
    }
}