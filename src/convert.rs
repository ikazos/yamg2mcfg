use std::collections::{HashSet, VecDeque, BTreeSet};

use crate::{mg::*, mcfg::*};



/// Check if the two given categories are eligible for merge1.
fn is_merge1(cat1: &Category, cat2: &Category) -> bool {
    //  merge1 (x, y) is valid iff...

    //  x is lexical, and
    (cat1.r#type == Type::Lexical) &&
    //  x is a singleton chain, and
    (cat1.tail_fseqs.is_empty()) &&
    //  The first item in x has at least one feature, and
    (!cat1.head_fseq.is_empty()) &&
    //  The first item in y has exactly one feature, and
    (cat2.head_fseq.len() == 1) &&
    //  There is some f, such that the first feature of the first item of x is
    //  =f and the first feature of the first item of y is f.
    (match (&cat1.head_fseq[0], &cat2.head_fseq[0]) {
        (Feature::Selector(x1), Feature::Selectee(x2)) => x1 == x2,
        _ => false,
    })
}



/// Check if the two given categories are eligible for merge2.
fn is_merge2(cat1: &Category, cat2: &Category) -> bool {
    //  merge2 (x, y) is valid iff...

    //  x is derived, and
    (cat1.r#type == Type::Derived) &&
    //  The first item in x has at least one feature, and
    (!cat1.head_fseq.is_empty()) &&
    //  The first item in y has exactly one feature, and
    (cat2.head_fseq.len() == 1) &&
    //  There is some f, such that the first feature of the first item of x is
    //  =f and the first feature of the first item of y is f.
    (match (&cat1.head_fseq[0], &cat2.head_fseq[0]) {
        (Feature::Selector(x1), Feature::Selectee(x2)) => x1 == x2,
        _ => false,
    })
}



/// Check if the two given categories are eligible for merge3.
fn is_merge3(cat1: &Category, cat2: &Category) -> bool {
    //  merge3 (x, y) is valid iff...

    //  The first item in x has at least one feature, and
    (!cat1.head_fseq.is_empty()) &&
    //  The first item in y has at least two features, and
    (cat2.head_fseq.len() >= 2) &&
    //  There is some f, such that the first feature of the first item of x is
    //  =f and the first feature of the first item of y is f.
    (match (&cat1.head_fseq[0], &cat2.head_fseq[0]) {
        (Feature::Selector(x1), Feature::Selectee(x2)) => x1 == x2,
        _ => false,
    })
}



/// Check if the given category is eligible for move1.
fn is_move1(cat: &Category) -> Option<usize> {
    //  move1 (x) is valid iff..

    //  The first item in x has at least one feature, and
    if cat.head_fseq.is_empty() {
        return None;
    }

    //  There is some f, such that the first feature of the first item of x is
    //  +f and there is a unique non-first item whose first feature is -f, and
    let movers: Vec<(usize, &Vec<Feature>)> = cat.tail_fseqs.iter()
        .enumerate()
        .filter(|&(_, fseq)| {
            (fseq.len() == 1) &&
            match (&cat.head_fseq[0], &fseq[0]) {
                (Feature::Licensor(x1), Feature::Licensee(x2)) => x1 == x2,
                _ => false,
            }
        })
        .collect();

    //  This non-first item only has one feature.
    if (movers.len() == 1) && (movers[0].1.len() == 1) {
        Some(movers[0].0)
    }
    else {
        None
    }
}



/// Check if the given category is eligible for move2.
fn is_move2(cat: &Category) -> Option<usize> {
    //  move2 (x) is valid iff..

    //  The first item in x has at least one feature, and
    if cat.head_fseq.is_empty() {
        return None;
    }

    //  There is some f, such that the first feature of the first item of x is
    //  +f and there is a unique non-first item whose first feature is -f, and
    let movers: Vec<(usize, &Vec<Feature>)> = cat.tail_fseqs.iter()
        .enumerate()
        .filter(|&(_, fseq)| {
            (fseq.len() >= 2) &&
            match (&cat.head_fseq[0], &fseq[0]) {
                (Feature::Licensor(x1), Feature::Licensee(x2)) => x1 == x2,
                _ => false,
            }
        })
        .collect();

    //  This non-first item has more than one feature.
    if (movers.len() == 1) && (movers[0].1.len() > 1) {
        Some(movers[0].0)
    }
    else {
        None
    }
}



fn try_merge1(cat1: &Category, cat2: &Category) -> Option<(Category, Rule<String, Category>)> {
    if is_merge1(&cat1, cat2) {
        let new_head_fseq = cat1.head_fseq[1..].to_vec();
        // assert!(!new_head_fseq.is_empty());

        let new_tail_fseqs = cat2.tail_fseqs.clone();

        let new_cat = Category {
            r#type: Type::Derived,
            head_fseq: new_head_fseq,
            tail_fseqs: new_tail_fseqs,
        };

        let lhs = new_cat.clone();
        let rhs = vec![ cat1.clone(), cat2.clone() ];
        let mut idxs = vec![
            vec![ IndexPair { nt_idx: 0, tup_idx: 0 },
                    IndexPair { nt_idx: 1, tup_idx: 0 } ]
        ];
        for tail_idx in 0..cat2.tail_fseqs.len() {
            idxs.push(vec![ IndexPair { nt_idx: 1, tup_idx: tail_idx + 1 } ]);
        }
        let new_rule = Rule::NonTerminal { lhs, rhs, idxs, comment: Some(format!("merge1")) };

        Some((new_cat, new_rule))
    }
    else {
        None
    }
}



fn try_merge2(cat1: &Category, cat2: &Category) -> Option<(Category, Rule<String, Category>)> {
    if is_merge2(&cat1, cat2) {
        let new_head_fseq = cat1.head_fseq[1..].to_vec();
        // assert!(!new_head_fseq.is_empty());

        let mut new_tail_fseqs = cat1.tail_fseqs.clone();
        new_tail_fseqs.extend(cat2.tail_fseqs.clone());

        let new_cat = Category {
            r#type: Type::Derived,
            head_fseq: new_head_fseq,
            tail_fseqs: new_tail_fseqs,
        };

        let lhs = new_cat.clone();
        let rhs = vec![ cat1.clone(), cat2.clone() ];
        let mut idxs = vec![
            vec![ IndexPair { nt_idx: 1, tup_idx: 0 },
                    IndexPair { nt_idx: 0, tup_idx: 0 } ]
        ];
        for tail_idx in 0..cat1.tail_fseqs.len() {
            idxs.push(vec![ IndexPair { nt_idx: 0, tup_idx: tail_idx + 1 } ]);
        }
        for tail_idx in 0..cat2.tail_fseqs.len() {
            idxs.push(vec![ IndexPair { nt_idx: 1, tup_idx: tail_idx + 1 } ]);
        }
        let new_rule = Rule::NonTerminal { lhs, rhs, idxs, comment: Some(format!("merge2")) };

        Some((new_cat, new_rule))
    }
    else {
        None
    }
}



fn try_merge3(cat1: &Category, cat2: &Category) -> Option<(Category, Rule<String, Category>)> {
    if is_merge3(&cat1, cat2) {
        let new_head_fseq = cat1.head_fseq[1..].to_vec();
        // assert!(!new_head_fseq.is_empty());

        let mut new_tail_fseqs = cat1.tail_fseqs.clone();
        new_tail_fseqs.insert(cat2.head_fseq[1..].to_vec());
        new_tail_fseqs.extend(cat2.tail_fseqs.clone());

        let new_cat = Category {
            r#type: Type::Derived,
            head_fseq: new_head_fseq,
            tail_fseqs: new_tail_fseqs,
        };

        let lhs = new_cat.clone();
        let rhs = vec![ cat1.clone(), cat2.clone() ];
        let mut idxs = vec![];
        for tup_idx in 0..cat1.tail_fseqs.len() + 1 {
            idxs.push(vec![ IndexPair { nt_idx: 0, tup_idx } ]);
        }
        for tup_idx in 0..cat2.tail_fseqs.len() + 1 {
            idxs.push(vec![ IndexPair { nt_idx: 1, tup_idx } ]);
        }
        let new_rule = Rule::NonTerminal { lhs, rhs, idxs, comment: Some(format!("merge3")) };

        Some((new_cat, new_rule))
    }
    else {
        None
    }
}



fn try_move1(cat: &Category) -> Option<(Category, Rule<String, Category>)> {
    if let Some(licensee_idx) = is_move1(&cat) {
        let new_head_fseq = cat.head_fseq[1..].to_vec();

        let mut new_tail_fseqs = cat.tail_fseqs.iter()
            .enumerate()
            .filter(|(k, _)| *k != licensee_idx)
            .map(|(_, tail_fseq)| tail_fseq)
            .cloned()
            .collect();

        let new_cat = Category {
            r#type: Type::Derived,
            head_fseq: new_head_fseq,
            tail_fseqs: new_tail_fseqs,
        };

        let lhs = new_cat.clone();
        let rhs = vec![ cat.clone() ];
        let mut idxs = vec![
            vec![
                IndexPair { nt_idx: 0, tup_idx: licensee_idx + 1 },
                IndexPair { nt_idx: 0, tup_idx: 0 },
            ]
        ];
        for tail_idx in 0..cat.tail_fseqs.len() {
            if tail_idx != licensee_idx {
                idxs.push(vec![ IndexPair { nt_idx: 0, tup_idx: tail_idx + 1 } ]);
            }
        }
        let new_rule = Rule::NonTerminal { lhs, rhs, idxs, comment: Some(format!("move1")) };

        Some((new_cat, new_rule))
    }
    else {
        None
    }
}



fn try_move2(cat: &Category) -> Option<(Category, Rule<String, Category>)> {
    if let Some(licensee_idx) = is_move2(&cat) {
        let new_head_fseq = cat.head_fseq[1..].to_vec();

        let mut new_tail_fseqs = cat.tail_fseqs.iter()
            .enumerate()
            .map(|(k, tail_fseq)| {
                if k != licensee_idx {
                    tail_fseq.clone()
                }
                else {
                    tail_fseq[1..].to_vec()
                }
            })
            .collect();

        let new_cat = Category {
            r#type: Type::Derived,
            head_fseq: new_head_fseq,
            tail_fseqs: new_tail_fseqs,
        };

        let lhs = new_cat.clone();
        let rhs = vec![ cat.clone() ];
        let mut idxs = vec![
            vec![
                IndexPair { nt_idx: 0, tup_idx: 0 },
            ]
        ];
        for tail_idx in 0..cat.tail_fseqs.len() {
            idxs.push(vec![ IndexPair { nt_idx: 0, tup_idx: tail_idx + 1 } ]);
        }
        let new_rule = Rule::NonTerminal { lhs, rhs, idxs, comment: Some(format!("move2")) };

        Some((new_cat, new_rule))
    }
    else {
        None
    }
}



/// Convert a MG into MCFG.
/// 
/// For more details on the conversion algorithm, see Matthieu Guillaumin's `mg2mcfg` report.
pub fn convert(mg: MG<String>) -> MCFG<String, Category> {
    //  Conversion.
    let mut cat_chart: HashSet<Category> = HashSet::new();
    let mut cat_agenda: VecDeque<Category> = VecDeque::new();
    let mut rules: HashSet<Rule<String, Category>> = HashSet::new();

    for lexical_entry in mg.lexicon.iter() {
        let cat = Category {
            r#type: Type::Lexical,
            head_fseq: lexical_entry.fseq.clone(),
            tail_fseqs: BTreeSet::new(),
        };

        let lhs = cat.clone();
        let rhs = lexical_entry.item.clone();

        cat_chart.insert(cat.clone());
        cat_agenda.push_back(cat);
        rules.insert(Rule::Terminal { lhs, rhs });
    }

    while let Some(cat1) = cat_agenda.pop_front() {
        // eprintln!("Convert agenda item {}", cat1);
        let mut cat_add_to_chart: Vec<Category> = vec![];

        for cat2 in cat_chart.iter() {
            //  merge1
            if let Some((new_cat, new_rule)) = try_merge1(&cat1, cat2) {
                if !cat_chart.contains(&new_cat) {
                    cat_add_to_chart.push(new_cat.clone());
                    cat_agenda.push_back(new_cat);
                }

                rules.insert(new_rule);
            }

            if let Some((new_cat, new_rule)) = try_merge1(cat2, &cat1) {
                if !cat_chart.contains(&new_cat) {
                    cat_add_to_chart.push(new_cat.clone());
                    cat_agenda.push_back(new_cat);
                }

                rules.insert(new_rule);
            }

            //  merge2
            if let Some((new_cat, new_rule)) = try_merge2(&cat1, cat2) {
                if !cat_chart.contains(&new_cat) {
                    cat_add_to_chart.push(new_cat.clone());
                    cat_agenda.push_back(new_cat);
                }

                rules.insert(new_rule);
            }

            if let Some((new_cat, new_rule)) = try_merge2(cat2, &cat1) {
                if !cat_chart.contains(&new_cat) {
                    cat_add_to_chart.push(new_cat.clone());
                    cat_agenda.push_back(new_cat);
                }

                rules.insert(new_rule);
            }

            //  merge3
            if let Some((new_cat, new_rule)) = try_merge3(&cat1, cat2) {
                if !cat_chart.contains(&new_cat) {
                    cat_add_to_chart.push(new_cat.clone());
                    cat_agenda.push_back(new_cat);
                }

                rules.insert(new_rule);
            }

            if let Some((new_cat, new_rule)) = try_merge3(cat2, &cat1) {
                if !cat_chart.contains(&new_cat) {
                    cat_add_to_chart.push(new_cat.clone());
                    cat_agenda.push_back(new_cat);
                }

                rules.insert(new_rule);
            }
        }

        //  move1
        if let Some((new_cat, new_rule)) = try_move1(&cat1) {
            if !cat_chart.contains(&new_cat) {
                cat_add_to_chart.push(new_cat.clone());
                cat_agenda.push_back(new_cat);
            }

            rules.insert(new_rule);
        }

        //  move2
        if let Some((new_cat, new_rule)) = try_move2(&cat1) {
            if !cat_chart.contains(&new_cat) {
                cat_add_to_chart.push(new_cat.clone());
                cat_agenda.push_back(new_cat);
            }

            rules.insert(new_rule);
        }

        cat_chart.extend(cat_add_to_chart);
    }

    //  Remove useless rules.
    let start_symbol = Category {
        r#type: Type::Derived,
        head_fseq: vec![ Feature::Selectee(mg.start_selectee.clone()) ],
        tail_fseqs: BTreeSet::new(),
    };
    let mut useful_cats: VecDeque<Category> = VecDeque::new();
    useful_cats.push_back(start_symbol.clone());

    let mut checked_useful_cats: HashSet<Category> = HashSet::new();
    
    while let Some(useful_cat) = useful_cats.pop_front() {
        for rule in rules.iter() {
            if let Rule::NonTerminal { lhs, rhs, .. } = rule {
                if lhs == &useful_cat {
                    for cat in rhs.iter() {
                        if !checked_useful_cats.contains(cat) {
                            useful_cats.push_back(cat.clone());
                        }
                    }
                }
            }
        }
        checked_useful_cats.insert(useful_cat);
    }

    let rules: HashSet<Rule<String, Category>> = rules.into_iter()
        .filter(|rule| {
            match rule {
                Rule::Terminal { lhs, .. } => checked_useful_cats.contains(lhs),
                Rule::NonTerminal { lhs, .. } => checked_useful_cats.contains(lhs),
            }
        })
        .collect();

    let mut terminals = HashSet::new();
    let mut nonterminals = HashSet::new();

    for rule in rules.iter() {
        match rule {
            Rule::Terminal { lhs, rhs } => {
                terminals.insert(rhs.clone());
                nonterminals.insert(lhs.clone());
            },

            Rule::NonTerminal { lhs, rhs, .. } => {
                nonterminals.insert(lhs.clone());
                for nt in rhs.iter() {
                    nonterminals.insert(nt.clone());
                }
            }
        }
    }

    let terminals = terminals.into_iter().collect();
    let nonterminals = nonterminals.into_iter().collect();
    let rules = rules.into_iter().collect();

    MCFG {
        terminals,
        nonterminals,
        rules,
        start_symbol,
    }
}



// #[test]
// fn test1() {
//     assert_eq!(is_move1(&Category {
//         r#type: Type::Derived,
//         head_fseq: vec![ Feature::Licensor(format!("wh")), Feature::Selectee(format!("c")) ],
//         tail_fseqs: vec![
//             vec![ Feature::Licensee(format!("wh")) ]
//         ],
//     }), Some(0));
// }