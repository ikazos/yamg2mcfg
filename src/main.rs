//! yamg2mcfg -- **y**et **a**nother **mg2mcfg**
//! 
//! Contains a MG-to-MCFG converter and MCFG parsers.



mod mg;
mod mcfg;
mod convert;
mod parser;



use std::{fs::File, io::{BufReader, BufRead, Read}, vec, path::PathBuf};

use log::{LevelFilter, info};
use argh::FromArgs;
use simplelog::{TermLogger, Config, TerminalMode, ColorChoice};

use crate::{mg::*, mcfg::*, parser::basic};



#[derive(FromArgs, Debug)]
/// Convert a MG into a MCFG, or parse with a converted MCFG.
struct Args {
    #[argh(subcommand)]
    subcommand: Subcommand,
}



#[derive(FromArgs, Debug)]
#[argh(subcommand)]
enum Subcommand {
    Convert(ConvertArgs),
    Parse(ParseArgs),
}



#[derive(FromArgs, Debug)]
/// Convert a MG into a MCFG.
#[argh(subcommand, name = "convert")]
struct ConvertArgs {
    #[argh(positional)]
    /// path to MG
    mg_path: PathBuf,

    #[argh(switch, short = 'v')]
    /// be verbose
    verbose: bool,

    #[argh(switch)]
    /// use JSON deserializer to parse MG
    deserialize: bool,
}



#[derive(FromArgs, Debug)]
/// Parse with a converted MCFG.
#[argh(subcommand, name = "parse")]
struct ParseArgs {
    #[argh(positional)]
    /// path to MCFG
    mcfg_path: PathBuf,

    #[argh(positional)]
    /// path to newline-delimited list of inputs
    inputs_path: PathBuf,

    #[argh(option, short = 'p')]
    /// type of parser (options: basic; default: basic)
    parser: Option<String>,

    #[argh(switch, short = 'v')]
    /// be verbose
    verbose: bool,
}



#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum Parser {
    Basic,
}



fn set_log_with_verbosity(verbose: bool) {
    TermLogger::init(
        if verbose { LevelFilter::Info } else { LevelFilter::Off },
        Config::default(),
        TerminalMode::Stderr,
        ColorChoice::Auto,
    ).unwrap();
}



fn cmd_convert(convert: ConvertArgs) {
    let ConvertArgs { mg_path, verbose, deserialize } = convert;
    
    set_log_with_verbosity(verbose);

    let mg_file = File::open(mg_path).unwrap();
    let mut mg_reader = BufReader::new(mg_file);

    let mg: MG<String> =
        if deserialize {
            serde_json::from_reader(mg_reader).unwrap()
        }
        else {
            let mut mg_string = String::new();
            mg_reader.read_to_string(&mut mg_string).unwrap();
            mg::parser::parse(mg_string).unwrap()
        };

    info!("Converting this MG:");
    info!(" *  Alphabet: {:?}", mg.alphabet);
    info!(
        " *  Features: [{}]",
        mg.features.iter()
            .map(|f| f.to_string())
            .reduce(|s1, s2| format!("{}, {}", s1, s2))
            .unwrap_or_else(|| format!(""))
    );
    info!(" *  Lexicon:");
    for lexical_entry in mg.lexicon.iter() {
        info!("     -  {:?} :: {}", lexical_entry.item, FeatureSliceDisplay(&lexical_entry.fseq));
    }
    info!(" *  Start selectee: {}", mg.start_selectee);

    let mcfg: MCFG<String, Category> = convert::convert(mg);

    info!("Converted MCFG:");
    info!(" *  Terminals: {:?}", mcfg.terminals);
    info!(" *  Nonterminals: {}", mcfg.nonterminals.iter()
        .map(|t| t.to_string())
        .reduce(|s1, s2| format!("{}, {}", s1, s2))
        .unwrap_or_else(|| format!(""))
    );
    info!(" *  Rules:");
    for rule in mcfg.rules.iter() {
        info!("     -  {}", rule);
    }
    info!(" *  Start symbol: {}", mcfg.start_symbol);

    let mcfg_string = serde_json::to_string(&mcfg).unwrap();
    println!("{}", mcfg_string);
}



fn cmd_parse(parse: ParseArgs) {
    let ParseArgs { mcfg_path, inputs_path, parser, verbose } = parse;
    
    set_log_with_verbosity(verbose);

    let parser = match parser.as_ref().map(|s| s.as_str()) {
        Some("basic") => Parser::Basic,
        None => Parser::Basic,
        Some(parser) => {
            panic!("Invalid parser specified: {}", parser);
        },
    };

    let mcfg_file = File::open(mcfg_path).unwrap();
    let mut mcfg_reader = BufReader::new(mcfg_file);
    let mut mcfg_string = String::new();
    mcfg_reader.read_to_string(&mut mcfg_string).unwrap();
    let mcfg: MCFG<String, Category> = serde_json::from_str(&mcfg_string).unwrap();

    let inputs_file = File::open(inputs_path).unwrap();
    let inputs_reader = BufReader::new(inputs_file);
    for input_line in inputs_reader.lines() {
        if let Ok(input_line) = input_line {
            let input: Vec<&str> = input_line.split(' ').collect();

            match parser {
                Parser::Basic => {
                    if let Some(parse) = basic::parse(&input, &mcfg) {
                        println!("{}", parse);
                    }
                    else {
                        println!("parse failed");
                    }
                },
            }
        }
    }
}



fn main() {
    let args: Args = argh::from_env();

    match args.subcommand {
        Subcommand::Convert(convert) => {
            cmd_convert(convert);
        },
        Subcommand::Parse(parse) => {
            cmd_parse(parse);
        },
    };
}