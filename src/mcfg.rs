use std::fmt;

use serde::{Serialize, Deserialize};



/// A pair of indices used in a MCFG rule that describe how the categories on
/// the RHS of this rule are linearized.
/// 
/// See the documentation for [`Rule`] to see how this type is used.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct IndexPair {
    /// The index of the non-terminal.
    pub nt_idx: usize,
    /// The index of the expression in the tuple.
    pub tup_idx: usize,
}



/// MCFG rule.
/// 
/// We follow the formulation of MCFG rules used in Albro 2000, Matthieu Guillaumin's `mg2mcfg` report, as well as Hale & Hunter's `ccpc`.  This formulation restricts the kinds of possible rules we can have in the MCFG, but the resulting space of possible rules contains everything we need for converting MGs to MCFGs.  The restricted rules can be represented in a simpler way under this formulation.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum Rule<T, NT> {
    /// A rule whose RHS is a single terminal.
    /// 
    /// If `lhs` is $ A $ and `rhs` is $ X $, then this rule represents $ A \rightarrow X $.
    /// 
    /// Generally, terminal MCFG rules can have multiple terminal symbols on the right-hand side.  For example, the following is a possible terminal MCFG rule for general MCFGs (written pedantically following Definition 6.1 in Kallmeyer 2010):
    /// 
    /// $$ A \rightarrow f [] $$
    /// 
    /// where:
    /// 
    /// $$ f () = \langle \text{the}, \text{dog} \rangle $$
    /// 
    /// However, in our formulation of MCFG rules, we only allow terminal rules to have exactly one terminal symbol on the right-hand side.  In other words, all non-terminal symbols that expand to only terminal symbols must have dimension 1.  So the rule above is not allowed in our formulation.  This is fine, since terminal MCFG rules are obtained from the conversion of MG lexical entries, which do not contain tuples of terminal symbols.  See [`convert`] for details on how the MG-to-MCFG conversion works.
    Terminal {
        lhs: NT,
        rhs: T,
    },
    /// A rule whose RHS is zero or more non-terminals.
    /// 
    /// If `lhs` is $ A $ and `rhs` is a vector of $ n $ non-terminals $ X_0, \cdots, X_{n-1} $, then `idxs` is a vector of $ \operatorname{dim} A $ vectors $ I_0, \cdots, I_{(\operatorname{dim} A) - 1} $, where each $ I_i $ is a vector of [`IndexPair`]s that describe how the components of the tuples represented by the non-terminals $ X_0, \cdots, X_{n-1} $ are concatenated to form $ A $.  Each [`IndexPair`] $ \langle \mathit{nt}, \mathit{tup} \rangle $ must satisfy $ 0 \leq \mathit{nt} < n $ and $ 0 \leq \mathit{tup} < \operatorname{dim} X_\mathit{nt} $.
    /// 
    /// You can provide an optional comment for the rule in the field `comment`.  The MG-to-MCFG conversion uses this field to indicate which MG operation was used to create each non-terminal MCFG rule; e.g., if a MCFG rule was derived via merge1, the comment will be `"merge1"`.
    /// 
    /// For example, the following rule:
    /// 
    /// $$ A \rightarrow f [ X_1, X_2, X_3 ] $$
    /// 
    /// where the dimensions of the non-terminal symbols are $ \operatorname{dim} A = 2, \operatorname{dim} X_1 = 2, \operatorname{dim} X_2 = 3, \operatorname{dim} X_3 = 4 $, and $ f $ is defined by:
    /// 
    /// $$ f ( \langle x_{11}, x_{12} \rangle, \langle x_{21}, x_{22}, x_{23} \rangle, \langle x_{31}, x_{32}, x_{33}, x_{34} \rangle ) = \langle x_{11} x_{21} x_{31} x_{12}, x_{22} x_{32} x_{23} x_{33} x_{34} \rangle $$
    /// 
    /// can be expressed as, for some `a`, `x1`, `x2`, `x3` of type `NT`:
    /// 
    /// ```
    /// Rule::NonTerminal {
    ///     lhs: a,
    ///     rhs: vec![ x1, x2, x3 ],
    ///     idxs: vec![
    ///         vec![ (0, 0), (1, 0), (2, 0), (0, 1) ],
    ///         vec![ (1, 1), (2, 1), (1, 2), (2, 2), (2, 3) ],
    ///     ],
    ///     comment: None,
    /// }
    /// ```
    /// 
    /// where `(nt, tup)` is the shorthand for `IndexPair { nt_idx: nt, tup_idx: tup }`.
    /// 
    /// Again, in general MCFGs, non-terminal rules can have a mix of terminal and non-terminal symbols on the right-hand side.  However, we restrict ourselves to only consider non-terminal rules with only non-terminal symbols on the right-hand side.
    NonTerminal {
        lhs: NT,
        rhs: Vec<NT>,
        idxs: Vec<Vec<IndexPair>>,
        comment: Option<String>,
    },
}



impl<T: fmt::Display, NT: fmt::Display> fmt::Display for Rule<T, NT> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Rule::Terminal { lhs, rhs } => {
                write!(f, "{} -> \"{}\"", lhs, rhs)
            },
            Rule::NonTerminal { lhs, rhs, idxs, comment } => {
                let fmt_rhs = rhs.iter()
                    .map(|nt| format!("{}", nt))
                    .reduce(|nt1, nt2| format!("{} {}", nt1, nt2))
                    .unwrap_or_else(|| format!(""));

                let fmt_idxs: String = idxs.iter()
                    .map(|idx_pairs| {
                        format!("[{}]", idx_pairs.iter()
                            .map(|idx_pair| format!("{},{}", idx_pair.nt_idx, idx_pair.tup_idx))
                            .reduce(|p1, p2| format!("{};{}", p1, p2))
                            .unwrap_or_else(|| format!(""))
                        )
                    })
                    .collect();

                write!(f, "{}: {} -> {} {}", comment.as_ref().map(|s| s.as_str()).unwrap_or_else(|| "(no name)"), lhs, fmt_rhs, fmt_idxs)
            },
        }
    }
}



/// Multiple Context-Free Grammar.
/// 
/// A Multiple Context-Free Grammar (MCFG) is a 4-tuple $ \langle T, N, R, S \rangle $ consisting of the set of terminal symbols $ T $, the set of non-terminal symbols $ N $, the set of rules $ R $ and the start symbol $ S \in N $.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct MCFG<T, NT> {
    pub terminals: Vec<T>,
    pub nonterminals: Vec<NT>,
    pub rules: Vec<Rule<T, NT>>,
    pub start_symbol: NT,
}